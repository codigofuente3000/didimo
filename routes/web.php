<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['prefix' => 'admin'], function () {

	Route::name('admin.index')->get('/', 'DashboardController@index');

	Route::group(['prefix' => 'datatables'], function () {
		Route::name('admin.datatables.categories')->get('categories', 'DatatablesController@categories');
		Route::name('admin.datatables.modules')->get('modules', 'DatatablesController@modules');
		Route::name('admin.datatables.products')->get('products', 'DatatablesController@products');
	    Route::name('admin.datatables.users')->get('users', 'DatatablesController@users');
	});

	Route::group(['prefix' => 'categories'], function () {
	    Route::name('admin.categories.index')->get('/', 'CategoriesController@index');
	    Route::name('admin.categories.create')->get('/create', 'CategoriesController@create');
	    Route::name('admin.categories.store')->post('/', 'CategoriesController@store');
	    Route::name('admin.categories.show')->get('/{category}', 'CategoriesController@show');
	    Route::name('admin.categories.edit')->get('/{category}/edit', 'CategoriesController@edit');
	    Route::name('admin.categories.update')->put('/{category}', 'CategoriesController@update');
	    Route::name('admin.categories.destroy')->delete('/{category}', 'CategoriesController@destroy');
	});

	Route::group(['prefix' => 'products'], function () {
	    Route::name('admin.products.index')->get('/', 'ProductsController@index');
	    Route::name('admin.products.create')->get('/create', 'ProductsController@create');
	    Route::name('admin.products.store')->post('/', 'ProductsController@store');
	    Route::name('admin.products.show')->get('/{product}', 'ProductsController@show');
	    Route::name('admin.products.edit')->get('/{product}/edit', 'ProductsController@edit');
	    Route::name('admin.products.update')->put('/{product}', 'ProductsController@update');
	    Route::name('admin.products.destroy')->delete('/{product}', 'ProductsController@destroy');
	});

	Route::group(['prefix' => 'modules'], function () {
	    Route::name('admin.modules.index')->get('/', 'ModulesController@index');
	    Route::name('admin.modules.create')->get('/create', 'ModulesController@create');
	    Route::name('admin.modules.store')->post('/', 'ModulesController@store');
	    Route::name('admin.modules.show')->get('/{module}', 'ModulesController@show');
	    Route::name('admin.modules.edit')->get('/{module}/edit', 'ModulesController@edit');
	    Route::name('admin.modules.update')->put('/{module}', 'ModulesController@update');
	    Route::name('admin.modules.destroy')->delete('/{module}', 'ModulesController@destroy');
	});

    Route::group(['prefix' => 'users'], function () {
	    Route::name('admin.users.index')->get('/', 'UsersController@index');
	    Route::name('admin.users.create')->get('/create', 'UsersController@create');
	    Route::name('admin.users.store')->post('/', 'UsersController@store');
	    Route::name('admin.users.show')->get('/{user}', 'UsersController@show');
	    Route::name('admin.users.edit')->get('/{user}/edit', 'UsersController@edit');
	    Route::name('admin.users.update')->put('/{user}', 'UsersController@update');
	    Route::name('admin.users.destroy')->delete('/{user}', 'UsersController@destroy');
	});

	Route::group(['prefix' => 'roles'], function () {
	    Route::name('admin.roles.index')->get('/', 'RolesController@index');
	});

	Route::group(['prefix' => 'timezones'], function () {
	    Route::name('admin.timezones.index')->get('/', 'TimezonesController@index');
	});

	Route::group(['prefix' => 'permissions'], function () {
	    Route::name('admin.permissions.index')->get('/', 'PermissionsController@index');
	});

});
