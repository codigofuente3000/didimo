<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function ($table) {

            if (!Schema::hasColumn('users', 'active')) {
                $table->boolean('active')->default(true)->after('id');
            }

            if (!Schema::hasColumn('users', 'timezone_id')) {
                $table->integer('timezone_id')->unsigned()->nullable()->after('active');
                $table->foreign('timezone_id')->references('id')->on('timezones')
                ->onUpdate('set null')->onDelete('set null');
            }

            if (!Schema::hasColumn('users', 'last_name')) {
                $table->string('last_name')->nullable()->after('name');
            }

            if (!Schema::hasColumn('users', 'avatar')) {
                $table->string('avatar')->nullable()->after('email')->default('users/default.png');
            }

            if (!Schema::hasColumn('users', 'language')) {
                $table->string('language')->nullable()->default('es')->after('avatar');
            }
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {

            if (Schema::hasColumn('users', 'active')) {
                $table->dropColumn('active');
            }

            if (Schema::hasColumn('users', 'timezone_id')) {
                $table->dropForeign(['timezone_id']);
                $table->dropColumn('timezone_id');
            }

            if (Schema::hasColumn('users', 'last_name')) {
                $table->dropColumn('last_name');
            }

            if (Schema::hasColumn('users', 'avatar')) {
                Schema::table('users', function ($table) {
                    $table->dropColumn('avatar');
                });
            }

            if (Schema::hasColumn('users', 'language')) {
                $table->dropColumn('language');
            }

        });

    }
}
