<?php

use App\Models\Shop\Product;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataType = Product::firstOrNew(['id' => 1]);
        if (!$dataType->exists) {
            $dataType->fill([
            	'name' 			=> 'Lorem',
            	'price' 		=> 60000,
            	'available' 	=> true,
            	'best_seller' 	=> true,
            	'img' 			=> 'http://lorempixel.com/200/100/food/',
            	'description' 	=> 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas eu.',
            ])->save();

            $dataType->categories()->sync([1,4]);
        }

        $dataType = Product::firstOrNew(['id' => 2]);
        if (!$dataType->exists) {
            $dataType->fill([
            	'name' 			=> 'ipsum',
            	'price' 		=> 20000,
            	'available' 	=> false,
            	'best_seller' 	=> false,
            	'img' 			=> 'http://lorempixel.com/200/100/food/',
            	'description' 	=> 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas eu.',
            ])->save();

            $dataType->categories()->sync([4]);
        }

        $dataType = Product::firstOrNew(['id' => 3]);
        if (!$dataType->exists) {
            $dataType->fill([
            	'name' 			=> 'dolor',
            	'price' 		=> 10000,
            	'available' 	=> true,
            	'best_seller' 	=> true,
            	'img' 			=> 'http://lorempixel.com/200/100/food/',
            	'description' 	=> 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas eu.',
            ])->save();

            $dataType->categories()->sync([4]);
        }

        $dataType = Product::firstOrNew(['id' => 4]);
        if (!$dataType->exists) {
            $dataType->fill([
            	'name' 			=> 'sit',
            	'price' 		=> 35000,
            	'available' 	=> false,
            	'best_seller' 	=> false,
            	'img' 			=> 'http://lorempixel.com/200/100/food/',
            	'description' 	=> 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas eu.',
            ])->save();

            $dataType->categories()->sync([1,2]);
        }

        $dataType = Product::firstOrNew(['id' => 5]);
        if (!$dataType->exists) {
            $dataType->fill([
            	'name' 			=> 'amet',
            	'price' 		=> 12000,
            	'available' 	=> true,
            	'best_seller' 	=> true,
            	'img' 			=> 'http://lorempixel.com/200/100/food/',
            	'description' 	=> 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas eu.',
            ])->save();

            $dataType->categories()->sync([1,4]);
        }

        $dataType = Product::firstOrNew(['id' => 6]);
        if (!$dataType->exists) {
            $dataType->fill([
            	'name' 			=> 'consectetur',
            	'price' 		=> 120000,
            	'available' 	=> true,
            	'best_seller' 	=> false,
            	'img' 			=> 'http://lorempixel.com/200/100/food/',
            	'description' 	=> 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas eu.',
            ])->save();

            $dataType->categories()->sync([1,4]);
        }

        $dataType = Product::firstOrNew(['id' => 7]);
        if (!$dataType->exists) {
            $dataType->fill([
            	'name' 			=> 'adipiscing',
            	'price' 		=> 50000,
            	'available' 	=> false,
            	'best_seller' 	=> false,
            	'img' 			=> 'http://lorempixel.com/200/100/food/',
            	'description' 	=> 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas eu.',
            ])->save();

            $dataType->categories()->sync([1,3]);
        }

        $dataType = Product::firstOrNew(['id' => 8]);
        if (!$dataType->exists) {
            $dataType->fill([
            	'name' 			=> 'elit',
            	'price' 		=> 2000,
            	'available' 	=> true,
            	'best_seller' 	=> false,
            	'img' 			=> 'http://lorempixel.com/200/100/food/',
            	'description' 	=> 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas eu.',
            ])->save();

            $dataType->categories()->sync([1,3]);
        }

        $dataType = Product::firstOrNew(['id' => 9]);
        if (!$dataType->exists) {
            $dataType->fill([
            	'name' 			=> 'Maecenas',
            	'price' 		=> 150000,
            	'available' 	=> true,
            	'best_seller' 	=> true,
            	'img' 			=> 'http://lorempixel.com/200/100/food/',
            	'description' 	=> 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas eu.',
            ])->save();

            $dataType->categories()->sync([2,4]);
        }

        $dataType = Product::firstOrNew(['id' => 10]);
        if (!$dataType->exists) {
            $dataType->fill([
            	'name' 			=> 'eu',
            	'price' 		=> 200000,
            	'available' 	=> false,
            	'best_seller' 	=> true,
            	'img' 			=> 'http://lorempixel.com/200/100/food/',
            	'description' 	=> 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas eu.',
            ])->save();

            $dataType->categories()->sync([2,3]);
        }


    }
}
