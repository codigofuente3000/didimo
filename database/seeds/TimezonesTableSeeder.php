<?php

use App\Models\Admin\Timezone;
use Illuminate\Database\Seeder;

class TimezonesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = Timezone::allJSON();

        foreach ($data as $tzs) {
            foreach ($tzs as $offset => $timezones) {
                foreach ($timezones as $timezone) {
                    Timezone::create([
                        'label'     => isset($timezone['label']) ? $timezone['label'] : null,
                        'value'     => isset($timezone['value']) ? $timezone['value'] : null,
                        'offset'    => isset($offset) ? $offset : null,
                    ]);
                }
            }
        }
    }
}
