<?php

use App\Models\Shop\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataType = Category::firstOrNew(['name' => 'Drinks']);
        if (!$dataType->exists) {
            $dataType->fill([
                'category_id' => 1,
            ])->save();
        }

        $dataType = Category::firstOrNew(['name' => 'Lunch']);
        if (!$dataType->exists) {
            $dataType->fill([
                'category_id' => 2,
            ])->save();
        }

        $dataType = Category::firstOrNew(['name' => 'Food']);
        if (!$dataType->exists) {
            $dataType->fill([
                'category_id' => 3,
            ])->save();
        }

        $dataType = Category::firstOrNew(['name' => 'Sea']);
        if (!$dataType->exists) {
            $dataType->fill([
                'category_id' => 4,
            ])->save();
        }
    }
}
