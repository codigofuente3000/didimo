<?php

use App\Models\Admin\Module;
use Illuminate\Database\Seeder;

class ModulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataType = Module::firstOrNew(['slug' => 'modules']);
        if (!$dataType->exists) {
            $dataType->fill([
                'name'                  => 'modules',
                'display_name_singular' => 'Module',
                'display_name_plural'   => 'Modules',
                'icon'                  => 'fa fa-puzzle-piece',
                'model_name'            => 'App\\Models\\Admin\\Module',
                'policy_name'           => '',
                'controller_name'       => 'ModulesController',
                'generate_permissions'  => 0,
                'description'           => '',
            ])->save();
        }

        $dataType = Module::firstOrNew(['slug' => 'timezones']);
        if (!$dataType->exists) {
            $dataType->fill([
                'name'                  => 'timezones',
                'display_name_singular' => 'Time Zone',
                'display_name_plural'   => 'Time Zones',
                'icon'                  => 'fa fa-clock-o',
                'model_name'            => 'App\\Models\\Admin\\Timezone',
                'policy_name'           => '',
                'controller_name'       => 'TimezonesController',
                'generate_permissions'  => 0,
                'description'           => '',
            ])->save();
        }

    	$dataType = Module::firstOrNew(['slug' => 'dashboard']);
        if (!$dataType->exists) {
            $dataType->fill([
                'name'                  => 'dashboard',
                'display_name_singular' => 'Dashboard',
                'display_name_plural'   => 'Dashboard',
                'icon'                  => 'fa fa-dashboard',
                'model_name'            => 'App\\Models\\Admin\\Dashboard',
                'policy_name'           => '',
                'controller_name'       => 'DashboardController',
                'generate_permissions'  => 0,
                'description'           => '',
            ])->save();
        }

        $dataType = Module::firstOrNew(['slug' => 'users']);
        if (!$dataType->exists) {
            $dataType->fill([
                'name'                  => 'users',
                'display_name_singular' => 'User',
                'display_name_plural'   => 'Users',
                'icon'                  => 'fa fa-users',
                'model_name'            => 'App\\Models\\Admin\\User',
                'policy_name'           => '',
                'controller_name'       => 'UsersController',
                'generate_permissions'  => 0,
                'description'           => '',
            ])->save();
        }

        $dataType = Module::firstOrNew(['slug' => 'roles']);
        if (!$dataType->exists) {
            $dataType->fill([
                'name'                  => 'roles',
                'display_name_singular' => 'Role',
                'display_name_plural'   => 'Roles',
                'icon'                  => 'fa fa-address-card',
                'model_name'            => 'App\\Models\\Admin\\Role',
                'policy_name'           => '',
                'controller_name'       => 'RolesController',
                'generate_permissions'  => 0,
                'description'           => '',
            ])->save();
        }

        $dataType = Module::firstOrNew(['slug' => 'permissions']);
        if (!$dataType->exists) {
            $dataType->fill([
                'name'                  => 'permissions',
                'display_name_singular' => 'Permission',
                'display_name_plural'   => 'Permissions',
                'icon'                  => 'fa fa-key',
                'model_name'            => 'App\\Models\\Admin\\Permission',
                'policy_name'           => '',
                'controller_name'       => 'PermissionsController',
                'generate_permissions'  => 0,
                'description'           => '',
            ])->save();
        }

        $dataType = Module::firstOrNew(['slug' => 'categories']);
        if (!$dataType->exists) {
            $dataType->fill([
                'name'                  => 'categories',
                'display_name_singular' => 'Category',
                'display_name_plural'   => 'Categories',
                'icon'                  => 'fa fa-list',
                'model_name'            => 'App\\Models\\Shop\\Category',
                'policy_name'           => '',
                'controller_name'       => 'CategoriesController',
                'generate_permissions'  => 0,
                'description'           => '',
            ])->save();
        }

        $dataType = Module::firstOrNew(['slug' => 'products']);
        if (!$dataType->exists) {
            $dataType->fill([
                'name'                  => 'products',
                'display_name_singular' => 'Product',
                'display_name_plural'   => 'Products',
                'icon'                  => 'fa fa-shopping-bag',
                'model_name'            => 'App\\Models\\Shop\\Product',
                'policy_name'           => '',
                'controller_name'       => 'ProductsController',
                'generate_permissions'  => 0,
                'description'           => '',
            ])->save();
        }

    }
}
