@if($errors->any())
    <div class="alert alert-danger alert-dismissable">
    	<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close"><i class="fa fa-times"></i></a>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif