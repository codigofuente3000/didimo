@if(session('status'))
    <div class="alert {{session('type')}} alert-dismissable">
    	<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close"><i class="fa fa-times"></i></a>
        <strong>{{ session('status') }}</strong>
    </div>
@endif