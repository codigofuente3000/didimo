@extends('layouts.adminlte.auth.auth')

@section('css')
@stop

@section('content')

<div class="login-box">
  <div class="login-logo">
    <a href="javascript:void(0)"><b>{{ __('generic.admin_panel') }}</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">{{ __('generic.sign_in') }}</p>

    @include('partials.alerts.errors')

    <form method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}
      <div class="form-group has-feedback">
        <input id="email" type="email" class="form-control" name="email" placeholder="{{ __('generic.email') }}" value="{{ old('email') }}" autocomplete="off" required autofocus>
        <span class="fa fa-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input id="password" type="password" class="form-control" name="password" placeholder="{{ __('generic.password') }}" required>
        <span class="fa fa-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-6">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> {{ __('generic.remember') }}
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <button type="submit" class="btn btn-primary btn-block btn-flat">{{ __('generic.sign_in') }}</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <a href="{{ route('password.request') }}">{{ __('generic.i_forgot_my_password') }}</a><br>
    <a href="{{ route('register') }}" class="text-center">{{ __('generic.register_user') }}</a>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

@stop

@section('javascript')
@stop
