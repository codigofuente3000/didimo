@extends('layouts.adminlte.auth.auth')

@section('css')
@stop

@section('content')

<div class="register-box">
  <div class="register-logo">
    <a href="javascript:void(0)"><b>{{ __('generic.admin_panel') }}</b></a>
  </div>

  <div class="register-box-body">
    <p class="login-box-msg">{{ __('generic.register_user') }}</p>

      @include('partials.alerts.errors')

      <form method="POST" action="{{ route('register') }}">
        {{ csrf_field() }}
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="{{ __('generic.full_name') }}" required autocomplete="off">
        <span class="fa fa-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="{{ __('generic.email') }}" required autocomplete="off">
        <span class="fa fa-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="password" placeholder="{{ __('generic.password') }}" required>
        <span class="fa fa-lock form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="password_confirmation" placeholder="{{ __('generic.confirm_password') }}" required>
        <span class="fa fa-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-6">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox" required> {{ __('generic.i_agree_to_the') }} <a href="javascript:void(0)">{{ __('generic.terms') }}</a>
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <button type="submit" class="btn btn-primary btn-block btn-flat">{{ __('generic.register_user') }}</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <a href="{{ route('login') }}" class="text-center">{{ __('generic.sign_in') }}</a>
  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->

@stop

@section('javascript')
@stop
