<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}"
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{ __('generic.admin_panel') }} | {{ __('generic.sign_in') }}</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  @include('layouts.adminlte.auth.partials.css')

</head>
<body class="hold-transition">

  @yield('content')

@include('layouts.adminlte.auth.partials.javascript')

</body>
</html>
