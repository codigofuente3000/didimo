@extends('layouts.adminlte.admin.master')

@section('page_title', __("generic.{$dataType->slug}"))

@section('css')
	@yield('read_css')
@stop

@section('header')
	@component('layouts.adminlte.admin.components.boxes.main_box', [
		'icon' 		=> 'fa fa-th',
		'is_solid' 	=> false,
		'title' 	=> __('generic.actions'),
		'type' 		=> 'danger',
	])
		<div class="container">
			@yield('read_header')
		</div>
	@endcomponent
@stop

@section('content')
	@component('layouts.adminlte.admin.components.boxes.main_box', [
		'icon' 		=> 'fa fa-eye',
		'is_solid' 	=> false,
		'title' 	=> __('generic.view_element'),
		'type' 		=> 'success',
	])
		<div class="container">
			@yield('read_content')
		</div>
	@endcomponent
@stop

@section('javascript')
	@yield('read_javascript')
@stop