@extends('layouts.adminlte.admin.master')

@section('page_title', !is_null($dataTypeContent->getKey()) ? __('generic.edit_element') : __('generic.add_element'))

@section('css')
	@yield('add_edit_css')
@stop

@section('header')
	@component('layouts.adminlte.admin.components.boxes.main_box', [
		'icon' 		=> 'fa fa-th',
		'is_solid' 	=> false,
		'title' 	=> __('generic.actions'),
		'type' 		=> 'danger',
	])
		<div class="container">
			@yield('add_edit_header')
		</div>
	@endcomponent
@stop

@section('content')
	@component('layouts.adminlte.admin.components.boxes.main_box', [
		'icon' 		=> 'fa fa-pencil',
		'is_solid' 	=> false,
		'title' 	=> !is_null($dataTypeContent->getKey()) ? __('generic.edit_element') : __('generic.add_element'),
		'type' 		=> 'info',
	])
		<div class="container">
			@yield('add_edit_content')
		</div>
	@endcomponent
@stop

@section('javascript')
	@yield('add_edit_javascript')
@stop