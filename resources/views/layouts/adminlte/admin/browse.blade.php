@extends('layouts.adminlte.admin.master')

@section('page_title', __("generic.{$dataType->slug}"))

@section('css')
	@yield('browse_css')
@stop

@section('header')
	@component('layouts.adminlte.admin.components.boxes.main_box', [
		'icon' 		=> 'fa fa-th',
		'is_solid' 	=> false,
		'title' 	=> __('generic.actions'),
		'type' 		=> 'danger',
	])
		<div class="col-xs-12">
			@yield('browse_header')
		</div>
	@endcomponent
@stop

@section('content')
	@component('layouts.adminlte.admin.components.boxes.main_box', [
		'icon' 		=> 'fa fa-list',
		'is_solid' 	=> false,
		'title' 	=> __('generic.list'),
		'type' 		=> 'primary',
	])
		<div class="col-xs-12">
			@include('partials.alerts.status')
			@yield('browse_content')
		</div>
	@endcomponent
@stop

@section('javascript')
	@yield('browse_javascript')
@stop