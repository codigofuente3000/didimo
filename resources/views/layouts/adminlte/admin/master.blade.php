<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>{{ env('APP_NAME') }} | @yield('page_title')</title>

  @include('layouts.adminlte.admin.partials.css')

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  @include('layouts.adminlte.admin.partials.topbar')

  @include('layouts.adminlte.admin.partials.sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    @include('layouts.adminlte.admin.partials.header')

    <!-- Main content -->
    <section class="content">

        @yield('header')

        @yield('content')

    </section>
    <!-- /.content -->

  </div>
  <!-- /.content-wrapper -->

  @include('layouts.adminlte.admin.partials.footer')

  @include('layouts.adminlte.admin.partials.control_sidebar')
</div>
<!-- ./wrapper -->

@include('layouts.adminlte.admin.partials.javascript')

</body>
</html>
