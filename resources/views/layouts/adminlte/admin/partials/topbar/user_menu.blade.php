  <!-- User Account: style can be found in dropdown.less -->
  <li class="dropdown user user-menu">
    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
      <img src="{{ asset('images/user.png') }}" class="user-image" alt="User Image">
      <span class="hidden-xs">{{ Auth::user()->text_fullname }}</span>
    </a>
    <ul class="dropdown-menu">
      <!-- User image -->
      <li class="user-header">
        <img src="{{ asset('images/user.png') }}" class="img-circle" alt="User Image">

        <p>
          {{ Auth::user()->text_fullname }}
          <small>{{ Auth::user()->email }}</small>
        </p>
      </li>
      <!-- Menu Body -->
      <li class="user-body">
        <div class="row">
          <div class="col-xs-4 text-center">
            <a href="javascript:void(0)"></a>
          </div>
          <div class="col-xs-4 text-center">
            <a href="javascript:void(0)"></a>
          </div>
          <div class="col-xs-4 text-center">
            <a href="javascript:void(0)"></a>
          </div>
        </div>
        <!-- /.row -->
      </li>
      <!-- Menu Footer-->
      <li class="user-footer">
        <div class="pull-left">
          <a href="javascript:void(0)" class="btn bg-navy btn-flat">Profile</a>
        </div>
        <div class="pull-right">
          <a href="{{ route('logout') }}" class="btn bg-red btn-flat" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Sign out</a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
        </div>
      </li>
    </ul>
  </li>