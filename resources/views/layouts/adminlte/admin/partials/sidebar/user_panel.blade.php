<!-- Sidebar user panel -->
<div class="user-panel">
  <div class="pull-left image">
    <img src="{{ asset('images/user.png') }}" class="img-circle" alt="User Image">
  </div>
  <div class="pull-left info">
    <p>{{ Auth::user()->text_fullname }}</p>
    <a href="javascript:void(0)"><i class="fa fa-circle text-success"></i> {{ __('generic.online') }}</a>
  </div>
</div>