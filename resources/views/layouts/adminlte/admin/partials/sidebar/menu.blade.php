      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">{{ __('generic.main_menu') }}</li>

        @permission('browse_dashboard')
            @component('layouts.adminlte.admin.components.menu.item', [
              'active'  => Route::is(['admin.index']),
              'icon'    => 'fa fa-dashboard',
              'route'   => route('admin.index'),
              'title'   => __('generic.dashboard'),
            ])
            @endcomponent
        @endpermission

        @permission(
            'browse_categories',
            'browse_products'
        )
            @component('layouts.adminlte.admin.components.menu.multiple', [
                'active'  => Route::is([
                    'admin.categories.*',
                    'admin.products.*',
                ]),
                'icon'    => 'fa fa-shopping-cart',
                'title'   => __('generic.shop'),
              ])

                @permission('browse_categories')
                    @component('layouts.adminlte.admin.components.menu.item', [
                      'active'  => Route::is(['admin.categories.*']),
                      'icon'    => 'fa fa-list',
                      'route'   => route('admin.categories.index'),
                      'title'   => __('generic.categories'),
                    ])
                    @endcomponent
                @endpermission

                @permission('browse_products')
                    @component('layouts.adminlte.admin.components.menu.item', [
                      'active'  => Route::is(['admin.products.*']),
                      'icon'    => 'fa fa-shopping-bag',
                      'route'   => route('admin.products.index'),
                      'title'   => __('generic.products'),
                    ])
                    @endcomponent
                @endpermission

            @endcomponent
        @endpermission
        
        @permission(
            'browse_modules',
            'browse_permissions',
            'browse_roles',
            'browse_timezones',
            'browse_users'
        )
            @component('layouts.adminlte.admin.components.menu.multiple', [
                'active'  => Route::is([
                    'admin.modules.*',
                    'admin.permissions.*',
                    'admin.roles.*',
                    'admin.timezones.*',
                    'admin.users.*',
                ]),
                'icon'    => 'fa fa-star',
                'title'   => __('generic.administration'),
              ])

                @permission('browse_users')
                    @component('layouts.adminlte.admin.components.menu.item', [
                      'active'  => Route::is(['admin.users.*']),
                      'icon'    => 'fa fa-users',
                      'route'   => route('admin.users.index'),
                      'title'   => __('generic.users'),
                    ])
                    @endcomponent
                @endpermission

                @permission('browse_roles')
                    @component('layouts.adminlte.admin.components.menu.item', [
                      'active'  => Route::is(['admin.roles.*']),
                      'icon'    => 'fa fa-address-card',
                      'route'   => route('admin.roles.index'),
                      'title'   => __('generic.roles'),
                    ])
                    @endcomponent
                @endpermission

                @permission('browse_permissions')
                    @component('layouts.adminlte.admin.components.menu.item', [
                      'active'  => Route::is(['admin.permissions.*']),
                      'icon'    => 'fa fa-key',
                      'route'   => route('admin.permissions.index'),
                      'title'   => __('generic.permissions'),
                    ])
                    @endcomponent
                @endpermission

                @permission('browse_timezones')
                    @component('layouts.adminlte.admin.components.menu.item', [
                      'active'  => Route::is(['admin.timezones.*']),
                      'icon'    => 'fa fa-clock-o',
                      'route'   => route('admin.timezones.index'),
                      'title'   => __('generic.timezones'),
                    ])
                    @endcomponent
                @endpermission

                @permission('browse_modules')
                    @component('layouts.adminlte.admin.components.menu.item', [
                      'active'  => Route::is(['admin.modules.*']),
                      'icon'    => 'fa fa-clock-o',
                      'route'   => route('admin.modules.index'),
                      'title'   => __('generic.modules'),
                    ])
                    @endcomponent
                @endpermission

            @endcomponent
        @endpermission

        </li>
      </ul>