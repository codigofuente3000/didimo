<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">

    @include('layouts.adminlte.admin.partials.sidebar.user_panel')

    @include('layouts.adminlte.admin.partials.sidebar.search_form')

    @include('layouts.adminlte.admin.partials.sidebar.menu')

  </section>
  <!-- /.sidebar -->
</aside>