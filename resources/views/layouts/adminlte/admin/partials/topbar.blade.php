<header class="main-header">
  <!-- Logo -->
  <a href="{{ route('admin.index') }}" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><b>5.5</b></span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><b>{{ env('APP_NAME') }}</b></span>
  </a>
  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <a href="javascript:void(0)" class="sidebar-toggle" data-toggle="push-menu" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>

    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">

        @include('layouts.adminlte.admin.partials.topbar.messages')

        @include('layouts.adminlte.admin.partials.topbar.notifications')

        @include('layouts.adminlte.admin.partials.topbar.tasks')

        @include('layouts.adminlte.admin.partials.topbar.user_menu')

        <!-- Control Sidebar Toggle Button -->
        <li>
          <a href="javascript:void(0)" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
        </li>
      </ul>
    </div>
  </nav>
</header>