<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <i class="{{ isset($dataType) ? $dataType->icon : null }}"></i> {{ isset($dataType) ? __("generic.{$dataType->slug}") : null }}
    <small>{{ isset($dataType) ? $dataType->description : null }}</small>
  </h1>
  <hr style="height:1px;border:none;color:#333;background-color:#333;" />
</section>