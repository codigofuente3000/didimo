@php

	$attr = [
		'icon' 		=> isset($icon) ? $icon : null,
		'is_solid'	=> isset($is_solid) && $is_solid === true ? 'box-solid' : null,
		'title' 	=> isset($title) ? $title : null,
		'type'		=> isset($type) ? 'box-'.$type : null,
	];

@endphp

<div class="box {{ $attr['is_solid'] }} {{ $attr['type'] }}">
	<div class="box-header with-border">
		<h3 class="box-title">
			<i class="{{ $attr['icon'] }}"></i>&nbsp;&nbsp;{{ $attr['title'] }}
		</h3>
	</div>
	<div class="box-body">{{ $slot }}</div>
</div>