@php

  $attr['active'] = isset($active) && $active === true ? 'active' : null;
  $attr['icon']   = isset($icon) ? $icon : null;
  $attr['title']  = isset($title) ? $title : null;

@endphp

<li class="{{ $attr['active'] }} treeview">
  <a href="javascript:void(0)">
    <i class="{{ $attr['icon'] }}"></i> <span>{{ $attr['title'] }}</span>
    <span class="pull-right-container">
      <i class="fa fa-angle-left pull-right"></i>
    </span>
  </a>
  <ul class="treeview-menu">
    {{ $slot }}
  </ul>
</li>