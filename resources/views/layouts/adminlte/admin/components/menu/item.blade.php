@php

	$attr['active'] = isset($active) && $active === true ? 'active' : null;
	$attr['icon'] 	= isset($icon) ? $icon : null;
	$attr['route']  = isset($route) ? $route : 'javascript:void(0)';
	$attr['title'] 	= isset($title) ? $title : null;

@endphp

<li class="{{ $attr['active'] }}">
	<a href="{{ $attr['route'] }}">
		<i class="{{ $attr['icon'] }}"></i> <span>{{ $attr['title'] }}</span>
	</a>
</li>