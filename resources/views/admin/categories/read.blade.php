@extends('layouts.adminlte.admin.read')

@section('read_css')
@stop

@section('read_header')
	
	<a href="{{ route('admin.'.$dataType->slug.'.index') }}" class="btn btn-primary"><i class="fa fa-list"></i> Regresar al Listado</a>

@stop

@section('read_content')

		<div>
			<h4>{{__('generic.name')}}</h4>
			<p class="text-muted">
				{{ isset($dataTypeContent->name) ? $dataTypeContent->name : null }}
			</p>
			<hr>
		</div>

@stop

@section('read_javascript')
@stop