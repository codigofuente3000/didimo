@extends('layouts.adminlte.admin.browse')

@section('browse_css')
@stop

@section('browse_header')

	<a href="{{ route('admin.'.$dataType->slug.'.create') }}" class="btn btn-success"><i class="fa fa-plus"></i> Agregar nuevo</a>

@stop

@section('browse_content')
	
	<table class="table" id="dataTable">
		<thead>
			<tr>
				<th>{{ __('generic.name') }}</th>
				<th>{{ __('generic.created_at') }}</th>
				<th>{{ __('generic.actions') }}</th>
			</tr>
		</thead>
		<tbody></tbody>
	</table>

@stop

@section('browse_javascript')

	<script type="text/javascript">
		
		var table = $('#dataTable').DataTable({
                order: [1,'desc'],
                autoWidth: false,
                language: {!! json_encode(__('datatable')) !!},
                processing: false,
                responsive: true,
                serverSide: true,
                ajax: '{{ route('admin.datatables.'.$dataType->slug) }}',
                columns: [
                    {data: "name", name: 'categories.name', searchable: true, orderable: true},
                	{data: "created_at", name: 'categories.created_at', searchable: true, orderable: true},
                    {data: "html_actions", name: null, searchable: false, orderable: false},
                ]
            });

	</script>

	<script src="{{ asset('js/delete.js') }}"></script>

@stop