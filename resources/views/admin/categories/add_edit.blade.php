@extends('layouts.adminlte.admin.add_edit')

@section('add_edit_css')
@stop

@section('add_edit_header')

	<a href="{{ route('admin.'.$dataType->slug.'.index') }}" class="btn btn-primary"><i class="fa fa-list"></i> Regresar al Listado</a>

@stop

@section('add_edit_content')

	<div class="row">
		
		<div class="col-xs-6 col-xs-offset-3">
			
			<form method="POST" action="{{ !is_null($dataTypeContent->getKey()) ? route('admin.'.$dataType->slug.'.update', $dataTypeContent->getKey()) : route('admin.'.$dataType->slug.'.store') }}" enctype="multipart/form-data">

			@include('partials.alerts.errors')

			<!-- PUT Method if we are editing -->
		    @if(!is_null($dataTypeContent->getKey()))
		        {{ method_field("PUT") }}
		    @endif

			<!-- CSRF TOKEN -->
			{{ csrf_field() }}


			<div class="form-group">
				<label for="name">{{ __('generic.name') }}</label>
				<input type="text" name="name" class="form-control" id="name" placeholder="{{ __('generic.name') }}" value="{{ is_null(old('name')) ? $dataTypeContent->name : old('name') }}" required autocomplete="off">
			</div>

			<hr>

			<div class="form-group">
				<button type="submit" class="btn btn-primary pull-right save">
					{!! !is_null($dataTypeContent->getKey()) ? "<i class='fa fa-pencil'></i>"." ".__('generic.edit') : "<i class='fa fa-save'></i>"." ".__('generic.save') !!}
				</button>	
			</div>

			</form>

		</div>

	</div>

	

@stop

@section('add_edit_javascript')
@stop