@extends('layouts.adminlte.admin.browse')

@section('browse_css')
@stop

@section('browse_header')
	
	<a href="{{ route('admin.'.$dataType->slug.'.create') }}" class="btn btn-success"><i class="fa fa-plus"></i> Agregar nuevo</a>

@stop

@section('browse_content')

	<table class="table" id="dataTable">
		<thead>
			<tr>
				<th>{{ __('generic.avatar') }}</th>
				<th>{{ __('generic.name') }}</th>
				<th>{{ __('generic.last_name') }}</th>
				<th>{{ __('generic.status') }}</th>
				<th>{{ __('generic.email') }}</th>
				<th>{{ __('generic.role') }}</th>
				<th>{{ __('generic.actions') }}</th>
			</tr>
		</thead>
		<tbody></tbody>
	</table>

@stop

@section('browse_javascript')
	
	<script type="text/javascript">
		
		var table = $('#dataTable').DataTable({
                order: [],
                autoWidth: false,
                language: {!! json_encode(__('datatable')) !!},
                processing: false,
                responsive: true,
                serverSide: true,
                ajax: '{{ route('admin.datatables.users') }}',
                columns: [
                	{data: "html_avatar", name: null, searchable: false, orderable: false},
                    {data: "name", name: 'users.name', searchable: true, orderable: true},
                    {data: "last_name", name: 'users.last_name', searchable: true, orderable: true},
                    {data: "html_active", name: 'users.active', searchable: true, orderable: true},
                    {data: "html_email", name: 'users.email', searchable: true, orderable: true},
                    {data: "text_role_display_name", name: null, searchable: false, orderable: false},
                    {data: "html_actions", name: null, searchable: false, orderable: false},
                ]
            });

	</script>

	<script src="{{ asset('js/delete.js') }}"></script>


@stop