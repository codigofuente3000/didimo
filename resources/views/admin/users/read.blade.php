@extends('layouts.adminlte.admin.read')

@section('read_css')
@stop

@section('read_header')
	
	<a href="{{ route('admin.'.$dataType->slug.'.index') }}" class="btn btn-primary"><i class="fa fa-list"></i> Regresar al Listado</a>

@stop

@section('read_content')

		<div>
			<h4>{{__('generic.avatar')}}</h4>
			<img src="{{ $dataTypeContent->text_avatar }}" style="width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;" />
			<hr>
		</div>

		<div>
			<h4>{{__('generic.name')}}</h4>
			<p class="text-muted">
				{{ isset($dataTypeContent->name) ? $dataTypeContent->name : null }}
			</p>
			<hr>
		</div>

		<div>
			<h4>{{__('generic.last_name')}}</h4>
			<p class="text-muted">
				{{ isset($dataTypeContent->last_name) ? $dataTypeContent->last_name : null }}
			</p>
			<hr>
		</div>

		<div>
			<h4>{{__('generic.active')}}</h4>
			<p class="text-muted">
				{!! isset($dataTypeContent->html_active) ? $dataTypeContent->html_active : null !!}
			</p>
			<hr>
		</div>

		<div>
			<h4>{{__('generic.email')}}</h4>
			<p class="text-muted">
				{!! isset($dataTypeContent->html_email) ? $dataTypeContent->html_email : null !!}
			</p>
			<hr>
		</div>

		<div>
			<h4>{{__('generic.role')}}</h4>
			<p class="text-muted">
				{{ isset($dataTypeContent->text_role_display_name) ? $dataTypeContent->text_role_display_name : null }}
			</p>
			<hr>
		</div>

		<div>
			<h4>{{__('generic.timezone')}}</h4>
			<p class="text-muted">
				{{ isset($dataTypeContent->text_timezone_label) ? $dataTypeContent->text_timezone_label : null }}
			</p>
			<hr>
		</div>

@stop

@section('read_javascript')
@stop