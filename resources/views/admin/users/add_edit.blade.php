@extends('layouts.adminlte.admin.add_edit')

@section('add_edit_css')
@stop

@section('add_edit_header')

	<a href="{{ route('admin.'.$dataType->slug.'.index') }}" class="btn btn-primary"><i class="fa fa-list"></i> Regresar al Listado</a>

@stop

@section('add_edit_content')

	<div class="row">
		
		<div class="col-xs-6 col-xs-offset-3">
			
			<form method="POST" action="{{ !is_null($dataTypeContent->getKey()) ? route('admin.'.$dataType->slug.'.update', $dataTypeContent->getKey()) : route('admin.'.$dataType->slug.'.store') }}" enctype="multipart/form-data">

			@include('partials.alerts.errors')

			<!-- PUT Method if we are editing -->
		    @if(!is_null($dataTypeContent->getKey()))
		        {{ method_field("PUT") }}
		    @endif

			<!-- CSRF TOKEN -->
			{{ csrf_field() }}


			<div class="form-group">
				<label for="name">{{ __('generic.name') }}</label>
				<input type="text" name="name" class="form-control" id="name" placeholder="{{ __('generic.name') }}" value="{{ is_null(old('name')) ? $dataTypeContent->name : old('name') }}" required autocomplete="off">
			</div>

			<div class="form-group">
				<label for="last_name">{{ __('generic.last_name') }}</label>
				<input type="text" name="last_name" class="form-control" id="last_name" placeholder="{{ __('generic.last_name') }}" value="{{ is_null(old('last_name')) ? $dataTypeContent->last_name : old('last_name') }}" autocomplete="off">
			</div>

			<div class="form-group">
				<label for="email">{{ __('generic.email') }}</label>
				<input type="email" name="email" class="form-control" id="email" placeholder="{{ __('generic.email') }}" value="{{ is_null(old('email')) ? $dataTypeContent->email : old('email') }}" required autocomplete="off">
			</div>

			<div class="form-group">
                <label for="role">{{ __('generic.role') }}</label>
                <select class="form-control select2" id="role" name="role">
                    <option value="">{{ __('generic.select') }}</option>
                    @foreach ($roles as $key => $value)
                    <option value="{{ $key }}"
                    	@if( isset($dataTypeContent->text_role_id) && $dataTypeContent->text_role_id == $key ) selected @endif >{{ $value }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="timezone">{{ __('generic.timezone') }}</label>
                <select class="form-control select2" id="timezone" name="timezone">
                    <option value="">{{ __('generic.select') }}</option>
                    @foreach ($timezones as $key => $value)
                    <option value="{{ $key }}"
                    @if( isset($dataTypeContent->timezone_id) && $dataTypeContent->timezone_id == $key ) selected @endif >{{ $value }}</option>
                    @endforeach
                </select>
            </div>

			<div class="form-group">
				<label for="password">{{ __('generic.password') }}</label>
				<input type="password" name="password" class="form-control" id="password" placeholder="{{ __('generic.password') }}" value="" autocomplete="new-password">
				@if(isset($dataTypeContent->password))
                    <small>{{ __('generic.password_hint') }}</small>
                @endif
			</div>

			<div class="form-group">
				<label for="password_confirmation">{{ __('generic.password_confirmation') }}</label>
				<input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="{{ __('generic.password_confirmation') }}" value="" autocomplete="new-password">
			</div>

			@if(!is_null($dataTypeContent->getKey()))
                <div class="form-group">
                    <label for="active">{{ __('generic.status') }}</label><br>
                    <input type="checkbox" name="active" id="active" class="toggleswitch"
                    data-on="{{ __('generic.active') }}" data-off="{{ __('generic.inactive') }}"
                    data-onstyle="success" data-offstyle="danger"
                    @if(isset($dataTypeContent->active) && $dataTypeContent->active) checked @endif>
                </div>
            @endif

			<hr>

			<div class="form-group">
				<button type="submit" class="btn btn-primary pull-right save">
					{!! !is_null($dataTypeContent->getKey()) ? "<i class='fa fa-pencil'></i>"." ".__('generic.edit') : "<i class='fa fa-save'></i>"." ".__('generic.save') !!}
				</button>	
			</div>

			</form>

		</div>

	</div>

	

@stop

@section('add_edit_javascript')
@stop