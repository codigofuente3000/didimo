@extends('layouts.adminlte.admin.browse')

@section('browse_css')

	<style type="text/css">
		
		.select2-selection__choice {
		    background-color: #0000bb !important;
		}

	</style>

@stop

@section('browse_header')

	<a href="{{ route('admin.'.$dataType->slug.'.create') }}" class="btn btn-success"><i class="fa fa-plus"></i> Agregar nuevo</a>

@stop

@section('browse_content')

	<div class="row">
		
		<div class="col-xs-12">
			
			@component('layouts.adminlte.admin.components.boxes.main_box', [
				'icon' 		=> 'fa fa-filter',
				'is_solid' 	=> false,
				'title' 	=> __('generic.filters'),
				'type' 		=> 'default',
			])

				<div class="row">
					
					<div class="col-md-6 col-xs-12">
						<label class="control-label label label-success">Condición</label>
						<br>
						<select class="form-control select2" id="price_range" name="price_range">
							<option value="=" selected>IGUAL QUE (=)</option>
							<option value="!=">NO IGUAL QUE (!=)</option>
							<option value=">">MAYOR QUE (>)</option>
							<option value=">=">MAYOR O IGUAL QUE (>=)</option>
							<option value="<">MENOR QUE (<)</option>
							<option value="<=">MENOR O IGUAL QUE (<=)</option>
						</select>
					</div>

					<div class="col-md-6 col-xs-12">
						<label class="control-label label label-success">Precio</label>
						<br>
						<input type="number" class="form-control" name="price" id="price">
					</div>

				</div>

				<div class="row">
					
					<div class="col-md-4 col-xs-12">
						<label class="control-label label label-success">{{ __('generic.available') }}</label>
						<br>
						<select class="form-control select2 change" id="available" name="available">
							<option value="" selected></option>
							<option value="1">{{ __('generic.yes') }}</option>
							<option value="0">{{ __('generic.no') }}</option>
						</select>
					</div>

					<div class="col-md-4 col-xs-12">
						<label class="control-label label label-success">{{ __('generic.best_seller') }}</label>
						<br>
						<select class="form-control select2 change" id="best_seller" name="best_seller">
							<option value="" selected></option>
							<option value="1">{{ __('generic.yes') }}</option>
							<option value="0">{{ __('generic.no') }}</option>
						</select>
					</div>

					<div class="col-md-4 col-xs-12">
						<label class="control-label label label-success">{{ __('generic.categories') }}</label>
						<br>
						<select class="form-control select2 change" id="categories" name="categories" multiple>
							@foreach($categories as $key => $value)
								<option value="{{ $key }}">{{ $value }}</option>
							@endforeach
						</select>
					</div>

				</div>

			@endcomponent

		</div>

	</div>

	<table class="table" id="dataTable">
		<thead>
			<tr>
				<th>{{ __('generic.image') }}</th>
				<th>{{ __('generic.created_at') }}</th>
				<th>{{ __('generic.name') }}</th>
				<th>{{ __('generic.price') }}</th>
				<th>{{ __('generic.available') }}</th>
				<th>{{ __('generic.best_seller') }}</th>
				<th>{{ __('generic.categories') }}</th>
				<th>{{ __('generic.actions') }}</th>
			</tr>
		</thead>
		<tbody></tbody>
	</table>

@stop

@section('browse_javascript')

	<script type="text/javascript">
		
		var table = $('#dataTable').DataTable({
                order: [1,'desc'],
                autoWidth: false,
                language: {!! json_encode(__('datatable')) !!},
                processing: false,
                responsive: true,
                serverSide: true,
                "ajax": {
		            url: '{{ route('admin.datatables.'.$dataType->slug) }}',
		            data: function (d) {
		            	d.available = $('#available').val();
		            	d.best_seller = $('#best_seller').val();
		            	d.categories = $('#categories').val();
		            	d.price = $('#price').val();
		            	d.price_range = $('#price_range').val();
		            }
		        },
                columns: [
                	{data: "html_image", name: 'products.img', searchable: false, orderable: false},
                	{data: "created_at", name: 'products.created_at', searchable: true, orderable: true},
                    {data: "name", name: 'products.name', searchable: true, orderable: true},
                    {data: "price", name: 'products.price', searchable: true, orderable: true},
                    {data: "html_available", name: 'products.available', searchable: true, orderable: true},
                    {data: "html_best_seller", name: 'products.best_seller', searchable: true, orderable: true},
                    {data: "html_categories", name: null, searchable: false, orderable: false},
                    {data: "html_actions", name: null, searchable: false, orderable: false},
                ]
            });

		$(document).on('change', '.change', function () {
			table.draw();
		});

		$(document).on('change', '#price_range', function () {
			check = $('#price').val() != '';

			if(check) {
		    	table.draw();
			}
		});

		$('#price').keyup(function() {
			check = $('#price_range').val() != '';

			if(check) {
		    	table.draw();
			}

		});

	</script>

	<script src="{{ asset('js/delete.js') }}"></script>
	<script src="{{ asset('js/cart.js') }}"></script>


@stop