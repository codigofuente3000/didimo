@extends('layouts.adminlte.admin.add_edit')

@section('add_edit_css')
	
	<style type="text/css">
		
		.select2-selection__choice {
		    background-color: #0000bb !important;
		}

	</style>

@stop

@section('add_edit_header')

	<a href="{{ route('admin.'.$dataType->slug.'.index') }}" class="btn btn-primary"><i class="fa fa-list"></i> Regresar al Listado</a>

@stop

@section('add_edit_content')

	<div class="row">
		
		<div class="col-xs-6 col-xs-offset-3">
			
			<form method="POST" action="{{ !is_null($dataTypeContent->getKey()) ? route('admin.'.$dataType->slug.'.update', $dataTypeContent->getKey()) : route('admin.'.$dataType->slug.'.store') }}" enctype="multipart/form-data">

			@include('partials.alerts.errors')

			<!-- PUT Method if we are editing -->
		    @if(!is_null($dataTypeContent->getKey()))
		        {{ method_field("PUT") }}
		    @endif

			<!-- CSRF TOKEN -->
			{{ csrf_field() }}


			<div class="form-group">
				<label for="name">{{ __('generic.name') }}</label>
				<input type="text" name="name" class="form-control" id="name" placeholder="{{ __('generic.name') }}" value="{{ is_null(old('name')) ? $dataTypeContent->name : old('name') }}" required autocomplete="off">
			</div>

			<div class="form-group">
				<label for="price">{{ __('generic.price') }}</label>
				<input type="number" name="price" class="form-control" id="price" placeholder="{{ __('generic.price') }}" value="{{ is_null(old('price')) ? $dataTypeContent->price : old('price') }}" required autocomplete="off">
			</div>

			<div class="form-group">
				<label for="image">URL de Imágen</label>
				<input type="text" name="img" class="form-control" id="img" placeholder="{{ __('generic.image') }}" value="{{ is_null(old('img')) ? $dataTypeContent->img : old('img') }}" autocomplete="off">
			</div>

			<div class="form-group">
            	<label for="categories">{{ __('generic.categories') }}</label><br>
				<select class="form-control select2 change" id="categories" name="categories[]" multiple>
					@foreach($categories as $key => $value)
						<option value="{{ $key }}" @if(in_array($key, $dataTypeContent->selected_categories)) selected @endif>{{ $value }}</option>
					@endforeach
				</select>
			</div>

            <div class="form-group">
				<label for="description">{{ __('generic.description') }}</label>
				<textarea class="form-control" name="description" id="description" placeholder="{{ __('generic.description') }}">{{ is_null(old('description')) ? $dataTypeContent->description : old('description') }}</textarea>
			</div>

			<div class="form-group">
                <label for="available">{{ __('generic.available') }}</label><br>
                <input type="checkbox" name="available" id="available" class="toggleswitch"
                data-on="{{ __('generic.yes') }}" data-off="{{ __('generic.no') }}"
                data-onstyle="success" data-offstyle="danger"
                @if(isset($dataTypeContent->available) && $dataTypeContent->available) checked @endif>
            </div>

            <div class="form-group">
                <label for="best_seller">{{ __('generic.best_seller') }}</label><br>
                <input type="checkbox" name="best_seller" id="best_seller" class="toggleswitch"
                data-on="{{ __('generic.yes') }}" data-off="{{ __('generic.no') }}"
                data-onstyle="success" data-offstyle="danger"
                @if(isset($dataTypeContent->best_seller) && $dataTypeContent->best_seller) checked @endif>
            </div>

            

			<hr>

			<div class="form-group">
				<button type="submit" class="btn btn-primary pull-right save">
					{!! !is_null($dataTypeContent->getKey()) ? "<i class='fa fa-pencil'></i>"." ".__('generic.edit') : "<i class='fa fa-save'></i>"." ".__('generic.save') !!}
				</button>	
			</div>

			</form>

		</div>

	</div>

	

@stop

@section('add_edit_javascript')
@stop