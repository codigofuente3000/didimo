@extends('layouts.adminlte.admin.read')

@section('read_css')
@stop

@section('read_header')

	<a href="{{ route('admin.'.$dataType->slug.'.index') }}" class="btn btn-primary"><i class="fa fa-list"></i> Regresar al Listado</a>

@stop

@section('read_content')

		<div>
			<h4>{{__('generic.image')}}</h4>
			<img src="{{ $dataTypeContent->img }}" style="width:300px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;" />
			<hr>
		</div>

		<div>
			<h4>{{__('generic.name')}}</h4>
			<p class="text-muted">
				{{ isset($dataTypeContent->name) ? $dataTypeContent->name : null }}
			</p>
			<hr>
		</div>

		<div>
			<h4>{{__('generic.price')}}</h4>
			<p class="text-muted">
				{{ isset($dataTypeContent->price) ? $dataTypeContent->price : null }}
			</p>
			<hr>
		</div>

		<div>
			<h4>{{__('generic.available')}}</h4>
			<p class="text-muted">
				{!! isset($dataTypeContent->html_available) ? $dataTypeContent->html_available : null !!}
			</p>
			<hr>
		</div>

		<div>
			<h4>{{__('generic.best_seller')}}</h4>
			<p class="text-muted">
				{!! isset($dataTypeContent->html_best_seller) ? $dataTypeContent->html_best_seller : null !!}
			</p>
			<hr>
		</div>

		<div>
			<h4>{{__('generic.categories')}}</h4>
			<p class="text-muted">
				{!! isset($dataTypeContent->html_categories) ? $dataTypeContent->html_categories : null !!}
			</p>
			<hr>
		</div>

		<div>
			<h4>{{__('generic.description')}}</h4>
			<p class="text-muted">
				{{ isset($dataTypeContent->description) ? $dataTypeContent->description : null }}
			</p>
			<hr>
		</div>

@stop

@section('read_javascript')
@stop