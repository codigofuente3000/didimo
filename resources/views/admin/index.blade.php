@extends('layouts.adminlte.admin.master')

@section('page_title', __("generic.{$dataType->slug}"))

@section('css')
@stop

@section('content')
	
      <!-- Small boxes (Stat box) -->

      <div class="row">
        
        <div class="col-lg-6 col-xs-6">
          <!-- small box -->
          <a href="{{ route('admin.categories.index') }}" class="small-box-link">
            <div class="small-box bg-navy">
              <div class="inner">
                <h3>{{ __('generic.categories') }}</h3>
                <p>&nbsp;</p>
              </div>
              <div class="icon" style="color: white">
                <i class="fa fa-list"></i>
              </div>
              <div class="small-box-footer">INGRESAR <i class="fa fa-arrow-circle-right"></i></div>
            </div>
          </a>
        </div>
        <!-- ./col -->

        <div class="col-lg-6 col-xs-6">
          <!-- small box -->
          <a href="{{ route('admin.products.index') }}" class="small-box-link">
            <div class="small-box bg-purple">
              <div class="inner">
                <h3>{{ __('generic.products') }}</h3>
                <p>&nbsp;</p>
              </div>
              <div class="icon" style="color: white">
                <i class="fa fa-shopping-bag"></i>
              </div>
              <div class="small-box-footer">INGRESAR <i class="fa fa-arrow-circle-right"></i></div>
            </div>
          </a>
        </div>
        <!-- ./col -->

      </div>


      <div class="row">

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <a href="{{ route('admin.users.index') }}" class="small-box-link">
            <div class="small-box bg-aqua">
              <div class="inner">
                <h3>Usuarios</h3>
                <p>&nbsp;</p>
              </div>
              <div class="icon" style="color: white">
                <i class="fa fa-users"></i>
              </div>
              <div class="small-box-footer">INGRESAR <i class="fa fa-arrow-circle-right"></i></div>
            </div>
          </a>
        </div>
        <!-- ./col -->

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <a href="{{ route('admin.roles.index') }}" class="small-box-link">
            <div class="small-box bg-green">
              <div class="inner">
                <h3>Roles</h3>
                <p>&nbsp;</p>
              </div>
              <div class="icon" style="color: white">
                <i class="fa fa-address-card"></i>
              </div>
              <div class="small-box-footer">INGRESAR <i class="fa fa-arrow-circle-right"></i></div>
            </div>
          </a>
        </div>
        <!-- ./col -->


        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <a href="{{ route('admin.permissions.index') }}" class="small-box-link">
            <div class="small-box bg-red">
              <div class="inner">
                <h3>Permisos</h3>
                <p>&nbsp;</p>
              </div>
              <div class="icon" style="color: white">
                <i class="fa fa-key"></i>
              </div>
              <div class="small-box-footer">INGRESAR <i class="fa fa-arrow-circle-right"></i></div>
            </div>
          </a>
        </div>
        <!-- ./col -->


        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <a href="{{ route('admin.modules.index') }}" class="small-box-link">
            <div class="small-box bg-yellow">
              <div class="inner">
                <h3>Módulos</h3>
                <p>&nbsp;</p>
              </div>
              <div class="icon" style="color: white">
                <i class="fa fa-puzzle-piece"></i>
              </div>
              <div class="small-box-footer">INGRESAR <i class="fa fa-arrow-circle-right"></i></div>
            </div>
          </a>
        </div>
        <!-- ./col -->

      </div>
      <!-- /.row -->

@stop

@section('javascript')
@stop