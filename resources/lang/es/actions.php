<?php

return [

	'delete' 	=> 'Eliminar',
	'edit' 		=> 'Editar',
	'history' 	=> 'Histórico',
	'view' 		=> 'Ver',

];
