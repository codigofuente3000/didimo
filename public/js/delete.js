$(document).on('click', '.delete', function(e) {
    e.preventDefault();

    let data = {
    	id: 	$(this).data('id'),
    	name: 	$(this).data('name'),
    	href: 	$(this).attr('href'),
    }

    Swal({
	  title: '¿Eliminar elemento '+data.name+'?',
	  text: "Será removido del sistema.",
	  type: 'question',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  confirmButtonText: 'Eliminar',
	  cancelButtonText: 'Cancelar'
	}).then((result) => {
	  if (result.value) {

		axios.delete(data.href)
		.then(function (response) {

			if(window.table !== undefined){
				window.table.draw()
			}

		    Swal(
		      '¡Elemento Eliminado!',
		      '',
		      'success'
		    )
        })
        .catch(function (error) {
			Swal(
			  '¡Ha ocurrido un Error!',
			  'Contacte al Administrador del Sistema.',
			  'error'
			)
        });

	  }
	})

});