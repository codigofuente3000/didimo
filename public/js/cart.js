$(document).on('click', '.cart', function(e) {
    e.preventDefault();

    let data = {
    	id: 	$(this).data('id'),
    	name: 	$(this).data('name'),
    	href: 	$(this).attr('href'),
    }

    Swal({
	  title: '¿Añadir '+data.name+' al carrito de compras?',
	  text: "",
	  type: 'question',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  confirmButtonText: 'Confirmar',
	  cancelButtonText: 'Cancelar'
	}).then((result) => {
	  if (result.value) {

		Swal(
	      '¡Producto añadido al Carrito de Compras!',
	      '',
	      'success'
	    )

	  }
	})

});