<?php

namespace App\Traits;

use Laratrust;

trait ActionsGenerator
{
	protected $attributes;

	protected function wrapper($attributes = [])
	{
		$this->attributes['color'] 		= isset($attributes['color']) ? $attributes['color'] : null;
		$this->attributes['data-id'] 	= isset($attributes['data-id']) ? $attributes['data-id'] : null;
		$this->attributes['data-name'] 	= isset($attributes['data-name']) ? $attributes['data-name'] : null;
		$this->attributes['icon'] 		= isset($attributes['icon']) ? $attributes['icon'] : null;
		$this->attributes['name'] 		= isset($attributes['name']) ? $attributes['name'] : null;
		$this->attributes['permission'] = isset($attributes['permission']) ? $attributes['permission'] : false;
        $this->attributes['selector']   = isset($attributes['selector']) ? $attributes['selector'] : null;
		$this->attributes['title'] 		= isset($attributes['title']) ? $attributes['title'] : null;
		$this->attributes['url'] 		= isset($attributes['url']) ? $attributes['url'] : 'javascript:void(0)';

		$wrapper = $this->attributes['permission']
					? "<a 
	        			class='btn btn-xs {$this->attributes['color']} {$this->attributes['selector']}'
	        			data-id='{$this->attributes['data-id']}'
	        			data-name='{$this->attributes['data-name']}'
	        			href='{$this->attributes['url']}'
	        			title='{$this->attributes['title']}'
	        		>
	        			<i class='{$this->attributes['icon']}'></i>
	           			<span class='hidden-sm hidden-xs'>{$this->attributes['name']}</span>
	        		</a>"
	        		: null;

	    return $wrapper;


	}

    protected function action_view($permission = false, $url = null, $id = null, $name = null)
    {
    	$attributes['permission']   = $permission;
        $attributes['url']          = $url;
        $attributes['data-id']      = $id;
        $attributes['data-name']    = $name;

        $attributes['color']        = "bg-green";
        $attributes['icon']         = "fa fa-eye";
        $attributes['selector']     = "view";
        $attributes['title']        = __('actions.view');

        return $this->wrapper($attributes);
    }

    protected function action_edit($permission = false, $url = null, $id = null, $name = null)
    {
        $attributes['permission']   = $permission;
        $attributes['url']          = $url;
        $attributes['data-id']      = $id;
        $attributes['data-name']    = $name;

        $attributes['color']        = "bg-blue";
        $attributes['icon']         = "fa fa-pencil";
        $attributes['selector']     = "edit";
        $attributes['title']        = __('actions.edit');

        return $this->wrapper($attributes);
    }

    protected function action_delete($permission = false, $url = null, $id = null, $name = null)
    {
        $attributes['permission']   = $permission;
        $attributes['url']          = $url;
        $attributes['data-id']      = $id;
        $attributes['data-name']    = $name;

        $attributes['color']        = "bg-red";
        $attributes['icon']         = "fa fa-trash";
        $attributes['selector']     = "delete";
        $attributes['title']        = __('actions.delete');

        return $this->wrapper($attributes);
    }

    protected function action_cart($permission = false, $url = null, $id = null, $name = null)
    {
        $attributes['permission']   = $permission;
        $attributes['url']          = $url;
        $attributes['data-id']      = $id;
        $attributes['data-name']    = $name;

        $attributes['color']        = "bg-purple";
        $attributes['icon']         = "fa fa-shopping-cart";
        $attributes['selector']     = "cart";
        $attributes['title']        = 'Añadir al Carrito';

        return $this->wrapper($attributes);
    }

}
