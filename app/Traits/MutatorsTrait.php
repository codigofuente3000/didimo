<?php

namespace App\Traits;

use Stevebauman\Purify\Facades\Purify;

trait MutatorsTrait
{
    public function setActiveAttribute($value)
    {
        $this->attributes['active'] = (isset($value) && $value == 'on') ?: false;
    }

    public function setAvatarAttribute($value)
    {
        if(isset($value)) {
            $this->attributes['avatar'] = Purify::clean($value);
        }
    }

    public function setEmailAttribute($value)
    {
        if(isset($value)) {
            $value = strtolower($value);
            $this->attributes['email'] = Purify::clean($value);
        }
    }

    public function setLastNameAttribute($value)
    {
        if(isset($value)) {
            $value = strtolower($value);
            $value = ucwords($value);
            $this->attributes['last_name'] = Purify::clean($value);
        }
    }

    public function setNameAttribute($value)
    {
        if(isset($value)) {
            $value = strtolower($value);
            $value = ucwords($value);
            $this->attributes['name'] = Purify::clean($value);
        }
    }

    public function setPasswordAttribute($value)
    {
        if(isset($value)) {
            $this->attributes['password'] = bcrypt($value);
        }
    }

    public function setPhoneAttribute($value)
    {
        if(isset($value)) {
            $value = strtolower($value);
            $this->attributes['phone'] = Purify::clean($value);
        }
    }
 
}