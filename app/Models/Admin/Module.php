<?php

namespace App\Models\Admin;

use App\Traits\ActionsGenerator;
use Illuminate\Database\Eloquent\Model;
use Laratrust;

class Module extends Model
{
    use ActionsGenerator;

    protected $slug = 'modules';

    protected $appends = [
        'html_actions',
    ];

    public function getHtmlActionsAttribute()
    {
        $key = $this->getKey();

        $actions[0] = $this->action_view(Laratrust::can("browse_{$this->slug}"), route("admin.{$this->slug}.show", $key));
        $actions[1] = $this->action_edit(Laratrust::can("edit_{$this->slug}"), route("admin.{$this->slug}.edit", $key));
        $actions[2] = $this->action_delete(Laratrust::can("delete_{$this->slug}"));

        return implode(" ", $actions);
    }
}
