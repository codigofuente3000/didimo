<?php

namespace App\Models\Admin;

use App\Traits\ActionsGenerator;
use App\Traits\MutatorsTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Storage;
use Laratrust;
use Laratrust\Traits\LaratrustUserTrait;

class User extends Authenticatable
{
    use ActionsGenerator;
    use LaratrustUserTrait;
    use MutatorsTrait;
    use Notifiable;

    protected $slug = 'users';

    protected $appends = [
        'html_actions',
        'html_active',
        'html_avatar',
        'html_email',
        'text_role_display_name',
        'text_role_id',
        'text_timezone_label',
    ];

    protected $casts = [
        //
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // RELATIONSHIPS

    public function timezone()
    {
        return $this->belongsTo(Timezone::class, 'timezone_id');
    }

    // ACCESSORS

    public function getHtmlActionsAttribute()
    {
        $key = $this->getKey();
        $name = $this->name;

        $actions[0] = $this->action_view(Laratrust::can("browse_{$this->slug}"), route("admin.{$this->slug}.show", $key), $key);
        $actions[1] = $this->action_edit(Laratrust::can("edit_{$this->slug}"), route("admin.{$this->slug}.edit", $key), $key);
        $actions[2] = $this->action_delete(Laratrust::can("delete_{$this->slug}"), route("admin.{$this->slug}.destroy", $key), $key, $name);

        return implode(" ", $actions);
    }

    public function getHtmlActiveAttribute()
    {
        $active = $this->getAttributeValue('active');

        $class = (isset($active) && $active) ? 'success' : 'danger';
        $title = (isset($active) && $active) ? __('generic.active') : __('generic.inactive');
        
        $html = "<span class='label label-{$class}'>{$title}</<span>";

        return $html;
    }

    public function getHtmlAvatarAttribute()
    {
        $route = $this->getAttributeValue('text_avatar');

        $html = "<img src='{$route}' class='img-circle' style='width:50px;height:50px'>";

        return $html;
    }

    public function getTextAvatarAttribute()
    {
        $avatar = $this->getAttributeValue('avatar');

        $url = (isset($avatar) && !empty($avatar)) ? $avatar : 'users/default.png';
        $path = Storage::disk('public')->url($url);

        return str_replace('\\', '/', $path);
    }

    public function getHtmlEmailAttribute()
    {
        $email = $this->getAttributeValue('email');

        $html = "<a href='mailto:{$email}'>{$email}</a>";

        return $html;
    }

    public function getTextFullnameAttribute()
    {
        $name = $this->getAttributeValue('name');
        
        $last_name = $this->getAttributeValue('last_name');

        return isset($last_name) ? "{$name} {$last_name}" : $name;
    }

    public function getTextRoleDisplayNameAttribute()
    {
        $relation = $this->getRelationValue('roles')->first();

        return isset($relation) ? $relation->display_name : null;
    }

    public function getTextRoleIdAttribute()
    {
        $relation = $this->getRelationValue('roles')->first();

        return isset($relation) ? $relation->id : null;
    }

    public function getTextTimezoneLabelAttribute()
    {
        $relation = $this->getRelationValue('timezone');

        return isset($relation) ? $relation->label : null;
    }
}
