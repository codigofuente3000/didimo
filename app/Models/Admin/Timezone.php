<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Timezone extends Model
{
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
    	//
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
    	//
    ];

    /**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'label',
		'value',
		'offset',
	];

	/**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'timezones';

	/**
	 * Get all the time zones from the JSON file.
	 *
	 * @return array
	 */
	public static function allJSON()
	{
		$route = dirname(__FILE__) . '/Data/timezones.json';

		return json_decode(file_get_contents($route), true);
	}
}
