<?php

namespace App\Models\Shop;

use App\Models\Shop\Product;
use App\Traits\ActionsGenerator;
use App\Traits\MutatorsTrait;
use Illuminate\Database\Eloquent\Model;
use Laratrust;

class Category extends Model
{
	use ActionsGenerator;
    use MutatorsTrait;

    protected $slug = 'categories';

    protected $appends = [
        'html_actions',
    ];

    protected $casts = [
        //
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    protected $primaryKey = 'category_id';

    // RELATIONSHIPS

    public function products()
    {
        return $this->belongsToMany(Product::class, 'category_product', 'category_id', 'product_id');
    }

    // ACCESSORS

    public function getHtmlActionsAttribute()
    {
        $key = $this->getKey();
        $name = $this->name;

        $actions[0] = $this->action_view(Laratrust::can("browse_{$this->slug}"), route("admin.{$this->slug}.show", $key), $key);
        $actions[1] = $this->action_edit(Laratrust::can("edit_{$this->slug}"), route("admin.{$this->slug}.edit", $key), $key);
        $actions[2] = $this->action_delete(Laratrust::can("delete_{$this->slug}"), route("admin.{$this->slug}.destroy", $key), $key, $name);

        return implode(" ", $actions);
    }
}
