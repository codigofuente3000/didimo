<?php

namespace App\Models\Shop;

use App\Models\Shop\Category;
use App\Traits\ActionsGenerator;
use App\Traits\MutatorsTrait;
use Illuminate\Database\Eloquent\Model;
use Laratrust;

class Product extends Model
{
	use ActionsGenerator;
    use MutatorsTrait;

    protected $slug = 'products';

    protected $appends = [
        'html_actions',
        'html_available',
        'html_best_seller',
        'html_image',
        'html_categories',
    ];

    protected $casts = [
        //
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    protected $primaryKey = 'id';

    // RELATIONSHIPS

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'category_product', 'product_id', 'category_id');
    }

    // ACCESSORS

    public function getHtmlActionsAttribute()
    {
        $key = $this->getKey();
        $name = $this->name;

        $actions[0] = $this->action_view(Laratrust::can("browse_{$this->slug}"), route("admin.{$this->slug}.show", $key), $key);
        $actions[1] = $this->action_cart(Laratrust::can("browse_{$this->slug}"), null, $key, $name);
        $actions[2] = $this->action_edit(Laratrust::can("edit_{$this->slug}"), route("admin.{$this->slug}.edit", $key), $key);
        $actions[3] = $this->action_delete(Laratrust::can("delete_{$this->slug}"), route("admin.{$this->slug}.destroy", $key), $key, $name);

        return implode(" ", $actions);
    }

    public function getHtmlAvailableAttribute()
    {
        $available = $this->getAttributeValue('available');

        $class = (isset($available) && $available) ? 'success' : 'danger';
        $title = (isset($available) && $available) ? __('generic.yes') : __('generic.no');
        
        $html = "<span class='label label-{$class}'>{$title}</<span>";

        return $html;
    }

    public function getHtmlBestSellerAttribute()
    {
        $best_seller = $this->getAttributeValue('best_seller');

        $class = (isset($best_seller) && $best_seller) ? 'success' : 'danger';
        $title = (isset($best_seller) && $best_seller) ? __('generic.yes') : __('generic.no');
        
        $html = "<span class='label label-{$class}'>{$title}</<span>";

        return $html;
    }

    public function getHtmlImageAttribute()
    {
        $route = $this->getAttributeValue('img');

        $html = "<img src='{$route}' class='center-block' style='width:100px;height:75px;border-radius: 25%'>";

        return $html;
    }

    public function getSelectedCategoriesAttribute()
    {
        return $this->categories()
                    ->pluck('categories.category_id')
                    ->toArray();
    }

    public function getImgAttribute($route)
    {
        if(!isset($route)) {
            return 'https://www.gumtree.com/static/1/resources/assets/rwd/images/orphans/a37b37d99e7cef805f354d47.noimage_thumbnail.png';
        }

        return $route;
    }

    public function getHtmlCategoriesAttribute()
    {
        $categories = $this->getRelationValue('categories');

        $html = '';

        foreach ($categories as $key => $value) {
        	$html .= "<li>{$value->name}</li>";
        }

        return $html;
    }

    
}
