<?php

namespace App\Http\Controllers;

use App\Models\Admin\Module;
use Illuminate\Http\Request;

class ModulesController extends Controller
{
    protected $dataType;

    protected $dataTypeContent;

    protected $isServerSide;

    protected $model;

    protected $slug = 'modules';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:browse_modules');
        $this->middleware('permission:read_modules', ['only' => ['show']]);
        $this->middleware('permission:edit_modules', ['only' => ['edit', 'update']]);
        $this->middleware('permission:add_modules', ['only' => ['create', 'store']]);
        $this->middleware('permission:delete_modules', ['only' => ['destroy']]);

        $this->dataType = Module::where('slug', $this->slug)->firstOrFail();

        $this->model = app($this->dataType->model_name);

        $this->dataTypeContent = $this->model;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("admin.{$this->slug}.browse", [
            'dataType'          => $this->dataType,
            'dataTypeContent'   => $this->dataTypeContent,
            'model'             => $this->model,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admin\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function show(Module $module)
    {
        dd($module);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admin\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function edit(Module $module)
    {
        dd($module);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Admin\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Module $module)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admin\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function destroy(Module $module)
    {
        //
    }
}
