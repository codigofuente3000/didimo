<?php

namespace App\Http\Controllers;

use App\Models\Admin\Module;
use App\Models\Admin\User;
use App\Models\Shop\Category;
use App\Models\Shop\Product;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class DatatablesController extends Controller
{
    public function products(Request $request)
    {
        $fields       = ['products.*'];
        $raws         = ['html_actions','html_available','html_best_seller','html_image','html_categories'];
        $relations    = ['categories'];
        $query        = Product::with($relations)->select($fields);

        if (isset($request->available)) {
           $query->where('available', $request->available);
        }

        if (isset($request->best_seller)) {
           $query->where('best_seller', $request->best_seller);
        }

        if (isset($request->categories)) {
            $query->whereHas('categories', function($query2) use ($request) {
                $query2->whereIn('categories.category_id', $request->categories);
            });
        }

        if (isset($request->price_range) && isset($request->price)) {
           $query->where('price', $request->price_range, $request->price);
        }



        return DataTables::of($query)->with($relations)->rawColumns($raws)->make();
    }

    public function categories()
    {
        $fields       = ['categories.*'];
        $raws         = ['html_actions'];
        $relations    = [];
        $query        = Category::with($relations)->select($fields);

        return DataTables::of($query)->with($relations)->rawColumns($raws)->make();
    }

	public function modules()
    {
		$fields       = ['modules.*'];
		$raws         = ['html_actions'];
		$relations    = [];
    	$query 		  = Module::with($relations)->select($fields);

    	return DataTables::of($query)->with($relations)->rawColumns($raws)->make();
    }

    public function users()
    {
		$fields       = ['users.*'];
		$raws         = ['html_actions', 'html_active', 'html_avatar', 'html_email'];
		$relations    = ['roles', 'timezone'];
    	$query 		  = User::with($relations)->select($fields);

    	return DataTables::of($query)->with($relations)->rawColumns($raws)->make();
    }
}
