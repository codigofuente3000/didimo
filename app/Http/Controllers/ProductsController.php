<?php

namespace App\Http\Controllers;

use App\Models\Admin\Module;
use App\Models\Shop\Category;
use App\Models\Shop\Product;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    protected $dataType;

    protected $dataTypeContent;

    protected $isServerSide;

    protected $model;

    protected $slug = 'products';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:browse_products');
        $this->middleware('permission:read_products', ['only' => ['show']]);
        $this->middleware('permission:edit_products', ['only' => ['edit', 'update']]);
        $this->middleware('permission:add_products', ['only' => ['create', 'store']]);
        $this->middleware('permission:delete_products', ['only' => ['destroy']]);

        $this->dataType = Module::where('slug', $this->slug)->firstOrFail();

        $this->model = app($this->dataType->model_name);

        $this->dataTypeContent = $this->model;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::orderBy('name', 'asc')->pluck('name', 'category_id')->toArray();

        return view("admin.{$this->slug}.browse", [
            'dataType'          => $this->dataType,
            'dataTypeContent'   => $this->dataTypeContent,
            'model'             => $this->model,
            'categories'        => $categories,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::orderBy('name', 'asc')->pluck('name', 'category_id')->toArray();

        return view("admin.{$this->slug}.add_edit", [
            'dataType'          => $this->dataType,
            'dataTypeContent'   => $this->dataTypeContent,
            'model'             => $this->model,
            'categories'        => $categories,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->dataTypeContent = $this->model;

        $data = $request->validate([
            'name'          => 'required|string|max:255',
            'price'         => 'required|string|max:255',
            'img'           => 'nullable|string|max:255',
            'description'   => 'nullable|string|max:10000',
            'available'     => 'nullable',
            'best_seller'   => 'nullable',
            'categories'    => 'required',
        ]);

        $this->dataTypeContent->name = isset($data['name']) ? $data['name'] : null;
        $this->dataTypeContent->price = isset($data['price']) ? $data['price'] : null;
        $this->dataTypeContent->img = isset($data['img']) ? $data['img'] : null;
        $this->dataTypeContent->description = isset($data['description']) ? $data['description'] : null;
        $this->dataTypeContent->available = isset($data['available']) ? true : false;
        $this->dataTypeContent->best_seller = isset($data['best_seller']) ? true : false;

        $saved = $this->dataTypeContent->save();

        if(!$saved) {
            return redirect()
                ->route("admin.{$this->slug}.create")
                ->with([
                    'status' => __('generic.error'),
                    'type' => 'alert-danger',
                ]);
        }

        $categories = isset($data['categories']) ? $data['categories'] : [];

        $this->dataTypeContent->categories()->sync($categories);

        return redirect()
            ->route("admin.{$this->slug}.index")
            ->with([
                'status' => __('generic.data_saved'),
                'type' => 'alert-success',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Shop\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        $this->dataTypeContent = $product;

        return view("admin.{$this->slug}.read", [
            'dataType'          => $this->dataType,
            'dataTypeContent'   => $this->dataTypeContent,
            'model'             => $this->model,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Shop\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $this->dataTypeContent = $product;

        $categories = Category::orderBy('name', 'asc')->pluck('name', 'category_id')->toArray();

        return view("admin.{$this->slug}.add_edit", [
            'dataType'              => $this->dataType,
            'dataTypeContent'       => $this->dataTypeContent,
            'model'                 => $this->model,
            'categories'            => $categories,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Shop\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $this->dataTypeContent = $product;

        $data = $request->validate([
            'name'          => 'required|string|max:255',
            'price'         => 'required|string|max:255',
            'img'           => 'nullable|string|max:255',
            'description'   => 'nullable|string|max:10000',
            'available'     => 'nullable',
            'best_seller'   => 'nullable',
            'categories'    => 'required',
        ]);

        $this->dataTypeContent->name = isset($data['name']) ? $data['name'] : null;
        $this->dataTypeContent->price = isset($data['price']) ? $data['price'] : null;
        $this->dataTypeContent->img = isset($data['img']) ? $data['img'] : null;
        $this->dataTypeContent->description = isset($data['description']) ? $data['description'] : null;
        $this->dataTypeContent->available = isset($data['available']) ? true : false;
        $this->dataTypeContent->best_seller = isset($data['best_seller']) ? true : false;

        $saved = $this->dataTypeContent->save();

        if(!$saved) {
            return redirect()
                ->route("admin.{$this->slug}.create")
                ->with([
                    'status' => __('generic.error'),
                    'type' => 'alert-danger',
                ]);
        }

        $categories = isset($data['categories']) ? $data['categories'] : [];

        $this->dataTypeContent->categories()->sync($categories);

        return redirect()
            ->route("admin.{$this->slug}.index")
            ->with([
                'status' => __('generic.data_saved'),
                'type' => 'alert-success',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Shop\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
    }
}
