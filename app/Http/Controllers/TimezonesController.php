<?php

namespace App\Http\Controllers;

use App\Models\Admin\Module;
use App\Models\Admin\Timezone;
use Illuminate\Http\Request;

class TimezonesController extends Controller
{
    protected $dataType;

    protected $dataTypeContent;

    protected $isServerSide;

    protected $model;

    protected $slug = 'timezones';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->dataType = Module::where('slug', $this->slug)->firstOrFail();

        $this->model = app($this->dataType->model_name);

        $this->dataTypeContent = $this->model;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("admin.{$this->slug}.browse", [
            'dataType'          => $this->dataType,
            'dataTypeContent'   => $this->dataTypeContent,
            'model'             => $this->model,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admin\Timezone  $timezone
     * @return \Illuminate\Http\Response
     */
    public function show(Timezone $timezone)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admin\Timezone  $timezone
     * @return \Illuminate\Http\Response
     */
    public function edit(Timezone $timezone)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Admin\Timezone  $timezone
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Timezone $timezone)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admin\Timezone  $timezone
     * @return \Illuminate\Http\Response
     */
    public function destroy(Timezone $timezone)
    {
        //
    }
}
