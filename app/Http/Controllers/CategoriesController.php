<?php

namespace App\Http\Controllers;

use App\Models\Admin\Module;
use App\Models\Shop\Category;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    protected $dataType;

    protected $dataTypeContent;

    protected $isServerSide;

    protected $model;

    protected $slug = 'categories';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:browse_categories');
        $this->middleware('permission:read_categories', ['only' => ['show']]);
        $this->middleware('permission:edit_categories', ['only' => ['edit', 'update']]);
        $this->middleware('permission:add_categories', ['only' => ['create', 'store']]);
        $this->middleware('permission:delete_categories', ['only' => ['destroy']]);

        $this->dataType = Module::where('slug', $this->slug)->firstOrFail();

        $this->model = app($this->dataType->model_name);

        $this->dataTypeContent = $this->model;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("admin.{$this->slug}.browse", [
            'dataType'          => $this->dataType,
            'dataTypeContent'   => $this->dataTypeContent,
            'model'             => $this->model,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.{$this->slug}.add_edit", [
            'dataType'          => $this->dataType,
            'dataTypeContent'   => $this->dataTypeContent,
            'model'             => $this->model,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->dataTypeContent = $this->model;

        $data = $request->validate([
            'name'  => 'required|string|max:255|unique:categories,name',
        ]);

        $this->dataTypeContent->name = isset($data['name']) ? $data['name'] : null;

        $saved = $this->dataTypeContent->save();

        if(!$saved) {
            return redirect()
                ->route("admin.{$this->slug}.create")
                ->with([
                    'status' => __('generic.error'),
                    'type' => 'alert-danger',
                ]);
        }

        return redirect()
            ->route("admin.{$this->slug}.index")
            ->with([
                'status' => __('generic.data_saved'),
                'type' => 'alert-success',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Shop\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        $this->dataTypeContent = $category;

        return view("admin.{$this->slug}.read", [
            'dataType'          => $this->dataType,
            'dataTypeContent'   => $this->dataTypeContent,
            'model'             => $this->model,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Shop\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $this->dataTypeContent = $category;

        return view("admin.{$this->slug}.add_edit", [
            'dataType'          => $this->dataType,
            'dataTypeContent'   => $this->dataTypeContent,
            'model'             => $this->model,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Shop\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $this->dataTypeContent = $category;

        $data = $request->validate([
            'name' => 'required|string|max:255|unique:categories,name,'.$this->dataTypeContent->category_id.',category_id',
        ]);

        $this->dataTypeContent->name = isset($data['name']) ? $data['name'] : null;

        $saved = $this->dataTypeContent->save();

        if(!$saved) {
            return redirect()
                ->route("admin.{$this->slug}.create")
                ->with([
                    'status' => __('generic.error'),
                    'type' => 'alert-danger',
                ]);
        }

        return redirect()
            ->route("admin.{$this->slug}.index")
            ->with([
                'status' => __('generic.data_saved'),
                'type' => 'alert-success',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Shop\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $category->delete();
    }
}
