<?php

namespace App\Http\Controllers;

use App\Models\Admin\Module;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    protected $dataType;

    protected $dataTypeContent;

    protected $isServerSide;

    protected $model;

    protected $slug = 'dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->dataType = Module::where('slug', $this->slug)->firstOrFail();

        $this->isServerSide = $this->dataType->server_side;

        $this->model = app($this->dataType->model_name);

        $this->dataTypeContent = $this->model;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.index', [
            'dataType'          => $this->dataType,
            'dataTypeContent'   => $this->dataTypeContent,
            'isServerSide'      => $this->isServerSide,
            'model'             => $this->model,
        ]);
    }
}
