<?php

namespace App\Http\Controllers;

use App\Models\Admin\Module;
use App\Models\Admin\Role;
use App\Models\Admin\Timezone;
use App\Models\Admin\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    protected $dataType;

    protected $dataTypeContent;

    protected $isServerSide;

    protected $model;

    protected $slug = 'users';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:browse_users');
        $this->middleware('permission:read_users', ['only' => ['show']]);
        $this->middleware('permission:edit_users', ['only' => ['edit', 'update']]);
        $this->middleware('permission:add_users', ['only' => ['create', 'store']]);
        $this->middleware('permission:delete_users', ['only' => ['destroy']]);

        $this->dataType = Module::where('slug', $this->slug)->firstOrFail();

        $this->model = app($this->dataType->model_name);

        $this->dataTypeContent = $this->model;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("admin.{$this->slug}.browse", [
            'dataType'          => $this->dataType,
            'dataTypeContent'   => $this->dataTypeContent,
            'model'             => $this->model,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::pluck('display_name', 'id')->toArray();
        $timezones = Timezone::pluck('label', 'id')->toArray();

        return view("admin.{$this->slug}.add_edit", [
            'dataType'          => $this->dataType,
            'dataTypeContent'   => $this->dataTypeContent,
            'model'             => $this->model,
            'roles'             => $roles,
            'timezones'         => $timezones,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->dataTypeContent = $this->model;

        $data = $request->validate([
            'name'      => 'required|string|max:255',
            'last_name' => 'nullable|string|max:255',
            'email'     => 'required|string|email|max:255|unique:users,email',
            'password'  => 'nullable|min:6|confirmed',
            'avatar'    => 'image|mimes:jpeg,png|max:10000',
            'role'      => 'required',
            'timezone'  => 'nullable',
            'active'    => 'nullable',
        ]);

        $this->dataTypeContent->name = isset($data['name']) ? $data['name'] : null;
        $this->dataTypeContent->last_name = isset($data['last_name']) ? $data['last_name'] : null;
        $this->dataTypeContent->email = isset($data['email']) ? $data['email'] : null;
        $this->dataTypeContent->password = isset($data['password']) ? $data['password'] : null;
        $this->dataTypeContent->timezone_id = isset($data['timezone']) ? $data['timezone'] : null;
        $this->dataTypeContent->active = isset($data['active']) ? $data['active'] : null;

        $saved = $this->dataTypeContent->save();

        if(!$saved) {
            return redirect()
                ->route("admin.{$this->slug}.create")
                ->with([
                    'status' => __('generic.error'),
                    'type' => 'alert-danger',
                ]);
        }

        $role = isset($data['role']) ? $data['role'] : [];

        $this->dataTypeContent->roles()->sync($role);

        return redirect()
            ->route("admin.{$this->slug}.index")
            ->with([
                'status' => __('generic.data_saved'),
                'type' => 'alert-success',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admin\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $this->dataTypeContent = $user;

        return view("admin.{$this->slug}.read", [
            'dataType'          => $this->dataType,
            'dataTypeContent'   => $this->dataTypeContent,
            'model'             => $this->model,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admin\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $this->dataTypeContent = $user;

        $roles = Role::pluck('display_name', 'id')->toArray();
        $timezones = Timezone::pluck('label', 'id')->toArray();

        return view("admin.{$this->slug}.add_edit", [
            'dataType'          => $this->dataType,
            'dataTypeContent'   => $this->dataTypeContent,
            'model'             => $this->model,
            'roles'             => $roles,
            'timezones'         => $timezones,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Admin\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $this->dataTypeContent = $user;

        $data = $request->validate([
            'name'      => 'required|string|max:255',
            'last_name' => 'nullable|string|max:255',
            'email'     => 'required|string|email|max:255|unique:users,email,'.$this->dataTypeContent->id,
            'password'  => 'nullable|min:6|confirmed',
            'avatar'    => 'image|mimes:jpeg,png|max:10000',
            'role'      => 'required',
            'timezone'  => 'nullable',
            'active'    => 'nullable',
        ]);

        $this->dataTypeContent->name = isset($data['name']) ? $data['name'] : null;
        $this->dataTypeContent->last_name = isset($data['last_name']) ? $data['last_name'] : null;
        $this->dataTypeContent->email = isset($data['email']) ? $data['email'] : null;
        $this->dataTypeContent->password = isset($data['password']) ? $data['password'] : null;
        $this->dataTypeContent->timezone_id = isset($data['timezone']) ? $data['timezone'] : null;
        $this->dataTypeContent->active = isset($data['active']) ? $data['active'] : null;

        $saved = $this->dataTypeContent->save();

        if(!$saved) {
            return redirect()
                ->route("admin.{$this->slug}.create")
                ->with([
                    'status' => __('generic.error'),
                    'type' => 'alert-danger',
                ]);
        }

        $role = isset($data['role']) ? $data['role'] : [];

        $this->dataTypeContent->roles()->sync($role);

        return redirect()
            ->route("admin.{$this->slug}.index")
            ->with([
                'status' => __('generic.data_saved'),
                'type' => 'alert-success',
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admin\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
    }
}
