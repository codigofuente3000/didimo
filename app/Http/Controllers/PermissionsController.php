<?php

namespace App\Http\Controllers;

use App\Models\Admin\Module;
use App\Models\Admin\Permission;
use Illuminate\Http\Request;

class PermissionsController extends Controller
{
    protected $dataType;

    protected $dataTypeContent;

    protected $isServerSide;

    protected $model;

    protected $slug = 'permissions';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:browse_permissions');
        $this->middleware('permission:read_permissions', ['only' => ['show']]);
        $this->middleware('permission:edit_permissions', ['only' => ['edit', 'update']]);
        $this->middleware('permission:add_permissions', ['only' => ['create', 'store']]);
        $this->middleware('permission:delete_permissions', ['only' => ['destroy']]);

        $this->dataType = Module::where('slug', $this->slug)->firstOrFail();

        $this->isServerSide = $this->dataType->server_side;

        $this->model = app($this->dataType->model_name);

        $this->dataTypeContent = $this->model;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("admin.{$this->slug}.browse", [
            'dataType'          => $this->dataType,
            'dataTypeContent'   => $this->dataTypeContent,
            'isServerSide'      => $this->isServerSide,
            'model'             => $this->model,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admin\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function show(Permission $permission)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admin\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function edit(Permission $permission)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Admin\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Permission $permission)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admin\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function destroy(Permission $permission)
    {
        //
    }
}
