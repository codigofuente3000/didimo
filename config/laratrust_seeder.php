<?php

return [
    'role_structure' => [
        'admin' => [
            'categories'    => 'b,r,e,a,d',
            'dashboard'     => 'b',
            'modules'       => 'b,r,e,a,d',
            'permissions'   => 'b,r,e,a,d',
            'products'      => 'b,r,e,a,d',
            'roles'         => 'b,r,e,a,d',
            'timezones'     => 'b,r,e,a,d',
            'users'         => 'b,r,e,a,d',
        ],
        'standard' => [
            'categories'    => 'b,r,e,a,d',
            'dashboard'     => 'b',
            'products'      => 'b,r,e,a,d',
        ],
    ],
    'permission_structure' => [
        //
    ],
    'permissions_map' => [
        'b' => 'browse',
        'r' => 'read',
        'e' => 'edit',
        'a' => 'add',
        'd' => 'delete',
    ]
];
